/*
    Load the expressjs module into our application and saved it in a variable called express
 */
const express = require('express');
/* 
    Creating a server is simplified using express. app is now our server. It creates an application that sues express and stores it as app.
*/
const app = express();
const port = 4000;

/* 
!    Middleware 
?        -It provides common services across the application.
*        -When a stream of data enters our code, we don't have to conduct the data and end step in order to parse the data into a JSON object. 
*        -It allows the handle of streaming the data. It automatically parses the data into JSON.
*        -app.use - Express modules method and will then allow us to use methods
!       -------------------------------------------------------------------
*        app.use(express.json()) - this line of code removes the req.on steps when receiving data from the  user
*/
app.use(express.json());

//Mock data
let users = [{
        username: 'Madrigal',
        email: 'fateReader@test.com',
        password: 'dontTalkAboutMe',
    },
    {
        username: 'Luigi',
        email: 'marites@test.com',
        password: 'numberOneMarites',
    },
];

let items = [{
        name: 'roses',
        price: 8.0,
        isActive: true,
    },
    {
        name: 'tulips',
        price: 6.0,
        isActive: true,
    },
];

//app.get(<endpoint>, <function for req and res>)
app.get('/', (req, res) => {
    //send is a combination of writeHead and end()
    res.send('Hello from my first express API');
    //res.status(200).send(<message>) - This is how you handle status codes and error codes
});

/* 
 Mini Activity: 5 mins

    >> Create a get route in Expressjs which will be able to send a message in the client:

        >> endpoint: /greeting
        >> message: 'Hello from Batch182-surname'

    >> Test in postman
    >> Send your response in hangouts
*/

app.get('/greetings', (req, res) => {
    res.send('Hello from Batch182-surname');
});

//* Retrieval of mock database
app.get('/users', (req, res) => {
    //res.send - converts it to stringify already, we don't neeed to use res.json()
    res.send(users);
    //res.json() - stringify method under response but since we're already stringifying it through the .send() method, we don't need to use this
});

app.post('/users', (req, res) => {
    let newUsers = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    };

    users.push(newUsers);

    res.send(users);
});

//DELETE method
app.delete('/users', (req, res) => {
    users.pop();
    res.send(users);
});

//PUT method
//update user's password
//:index - wildcard. It specifies what users are we getting through the use of index. We're specifying who do we want to update. Wildcards are similar to a variable where it holds the user input
app.put('/users/:property/:index', (req, res) => {
    console.log(req.body);
    console.log(req.params);

    let usersIndex = Number(req.params.index);

    if (req.params.property === 'password') {
        /*
         *           - First off, we're accessing the mock data array which is called "users". In order to access the individual array, we need to use an index.
         *           - Next, we're accessing the array index based on the users' endpoint (through :index) and converting it to a number by using "+".
         *           -After accessing the array index, we're now accessing the array object index' property by using the dot notatation and specifying the ".password".
         *           -After accessing the object we want to change, we can now change its value to whatever the user has entered through the req.body.
         */
        users[usersIndex].password = req.body.password;
        res.send(users);
    } else if (req.params.property === 'username') {
        users[usersIndex].username = req.body.username;
        res.send(users[usersIndex]);
    }
});

//Retrieval of a single user
app.get('/users/getSingleUser/:index', (req, res) => {
    console.log(req.params);

    let index = Number(req.params.index);
    console.log(index);
    res.send(users[index]);
});

/* 
!    Activity:
*/

//GET ALL items
app.get('/items', (req, res) => {
    res.send(items);
});

//ADD new item
app.post('/items', (req, res) => {
    let newItems = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive,
    };

    items.push(newItems);
    res.send(items);
});

//UPDATE an item
app.put('/items/:index', (req, res) => {
    items[+req.params.index].price = req.body.price;
    res.send(items[+req.params.index]);
});

/*
Activity #2
>> Create a new route with '/items/getSingleItem/:index' endpoint. 
    >> This route should allow us to GET the details of a single item.
        -Pass the index number of the item you want to get via the url as url params in your postman.
        -http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
    
    >> Send the request and back to your api
        -get the data from the url params.
        -send the particular item from our array using its index to the client.

>> Create a new route to update an item with '/items/archive/:index' endpoint.    (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to de-activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
        -send the updated item in the client

>> Create a new route to update an item with '/items/activate/:index' endpoint. (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
        -send the updated item in the client

>> Create a new collection in Postman called s34-activity
>> Save the Postman collection in your s33-s34 folder
*/

/* 
>> Create a new route to update an item with '/items/activate/:index' endpoint. (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
        -send the updated item in the client

*/
app.get('/items/getSingleItem/:item', (req, res) => {
    res.send(items[+req.params.item]);
});

app.put('/items/archive/:index', (req, res) => {
    //Converting it into a number
    let itemIndex = parseInt(req.params.index);

    items[itemIndex].isActive = false;
    res.send(items[itemIndex]);
});

app.put('/items/activate/:index', (req, res) => {
    items[+req.params.index].isActive = true;
    res.send(items[+req.params.index]);
});

app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});